﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransicionNumerica : MonoBehaviour 
{
    public int numSimple;
    public int numTransicion;
    public Text txtSimple;
    public Text txtTransicion;
    private bool isCalculandoS = false;
    private bool isCalculandoT = false;
    float lerp = 0f, duracion = 2f;
    public Tipo tipo;
    public enum Tipo
    {
        Lerp,
        While
    }

    int scoreS = 1000;

    public void InitSimple()
    {
        ProcesarSimple();
    }

    public void InitTransicion()
    {
        ProcesarTransicion();
    }

    private void ProcesarSimple()
    {
        isCalculandoS = true;
    }

    private void ProcesarTransicion()
    {
        isCalculandoT = true;
    }

    private void Update()
    {
        if(tipo == Tipo.Lerp)
        {
            if (isCalculandoS)
            {
                txtSimple.text = numSimple.ToString();
            }

            if (isCalculandoT)
            {
                int score = 1000;
                int scoreTo = numTransicion;
                lerp += Time.deltaTime / duracion;
                score = (int)Mathf.Lerp(score, scoreTo, lerp);
                txtTransicion.text = score.ToString();
            }


            if (txtSimple.text.Equals(numSimple)) isCalculandoS = false;
            if (txtTransicion.text.Equals(numTransicion)) isCalculandoT = false;
        }
        if(tipo== Tipo.While)
        {
            while(!txtSimple.text.Equals(numSimple))
            {
                txtSimple.text = numSimple.ToString();
                isCalculandoS = true;
            }

            while (!txtTransicion.text.Equals(numTransicion))
            {
                int scoreTo = numTransicion;
                lerp += Time.deltaTime / duracion;
                scoreS = (int)Mathf.Lerp(scoreS, scoreTo, lerp);
                txtTransicion.text = scoreS.ToString();
                isCalculandoT = true;
            }
        }
    }
}
